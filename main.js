
// Группировка точек по координатам Y
function groupPointsByY(pointsArr) {
    const pointsDictByY = {};
    pointsArr.forEach( ({x, y}) => { y in pointsDictByY ? pointsDictByY[y].push(x) : pointsDictByY[y] = [x] })
    return pointsDictByY;
}

// Удаление из массива элементов, равных среднему значению и постсортировка
function removeAveragedValuesAndSort(arr, average){
    return arr
        .filter( x => x !== average )
        .sort( (x1, x2) => x1 > x2 )
}

// Симметричен ли массив
function isArraySymmetry(arr, average) {
    let symmetry = true;
    sortedArr = removeAveragedValuesAndSort(arr, average);
    const len = sortedArr.length
    if (len & 1) {
        symmetry = false;
    } else {
        for (let i=0; i < len / 2; i++ ){
            if ( (sortedArr[i] + sortedArr[len - i-1])/2 !== average ) {
                symmetry = false;
                break;
            }
        }
    }
    return symmetry;
}

// Проверка симметричности относительно точки
function checkVerticalLineSymmetry(pointsArr, coordXOfLine){
    const pointsDictByY = groupPointsByY(pointsArr);
    let symmetry = true;
    Object.entries(pointsDictByY)
        .forEach( ([, arrX]) => {
            if (!isArraySymmetry(arrX, coordXOfLine)){
                symmetry = false;
            }
        })
    return symmetry;
}

// Проверка на симметричность
function isPointsHaveSymmetry(pointsArr) {
    const averageArr = getAverages(pointsArr);
    return averageArr.length > 1
        ? checkVerticalLineSymmetry(pointsArr, averageArr[0])
        : true // Пустой массив и массив из одного элемента симметричны
}

// Получение массива средних значений точек, сгруппированных по Y
function getAverages(pointsArr) {
    const pointsDictByY = groupPointsByY(pointsArr);
    const averageArr = [];
    Object.entries(pointsDictByY).forEach(
        ([,arrX]) => averageArr.push(arrX.reduce((a, acc) => a + acc, 0)/arrX.length)
    )
    return averageArr;
}

// Идентичны ли средние значения
function isPointsHaveEqualAverages(pointsArr) {
    const averageArr = getAverages(pointsArr);
    let t = !averageArr.some( x => x !== averageArr[0] );
    return averageArr.length > 1
        ? !averageArr.some( x => x !== averageArr[0] )
        // Пустой массив имеет идентичные средние значения
        : true;
}

// Этап 1. Всегда возвращает true
// Этап 2. Должен быть один элемент а массиве
// Этап 3. Одинаковые координаты Y
// Этап 4. Сумма координат x в пределах одного y должна быть равна 0
// Этап 5. Для всех y среднее x должно совпадать
// + у каждой точки не лежащей посередине должна быть симметричная
// ей точка относительно среднего значения
const magicFunction = (pointsArr) =>
    isPointsHaveEqualAverages(pointsArr)
        ? isPointsHaveSymmetry(pointsArr)
        : false

module.exports = magicFunction