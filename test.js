var magicFunction = require('./main.js');

const suite = [
    // Этап 1
    {
        input: [{x:1, y: 0}],
        output: true,
    },
    {
        input: [{x:2, y: -32}],
        output: true,
    },
    // Этап 2
    {
        input: [{x:-2, y: 2}, {x:2, y: 1}],
        output: false,
    },
    // Этап 3
    {
        input: [{x:-2, y: 1}, {x:2, y: 1}],
        output: true,
    },
    // Этап 4
    {
        input: [{x:-2, y: 1}, {x:2, y: 1}, {x:-3, y: 15}, {x:3, y: 15}, {x: 0, y: 15}],
        output: true,
    },
    // Этап 5
    {
        input: [
            {x:-2, y: 1},
            {x:2, y: 1},
            {x:-3, y: 15},
            {x:3, y: 15},
            {x: 8, y: 15},
            {x: -3, y: 15},
            {x: -5, y: 15},
        ],
        output: false,
    },
    {
        input: [
            {x:0, y: 1},
            {x:4, y: 1},
            {x:-3, y: 15},
            {x:7, y: 15},
            {x: 2, y: 15},
        ],
        output: true,
    },
]

suite.forEach( ({ input, output }, index) => {
    if ( magicFunction(input) !== output ){
        throw `test ${index} failed`
    }
})

console.log('suite was successful');
